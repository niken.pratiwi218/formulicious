import React from 'react';
import { View, Text, TextInput, StyleSheet, Image, Button, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class AboutUs extends React.Component{

  logoutHandler() {
    this.props.navigation.navigate('LoginPage')
}  

  render(){
    console.log(this.props)
    return (
      <View style={styles.container}>
          <View style={styles.logo}>
          <Image source={require('./image/Logo.png')} style={{width: 240, height:65}} />
          </View> 
      
          <Text style={styles.versionText}>version 1.0-android</Text>
          <Text style={styles.versionText}> © Formulicious Id.</Text>

          <View style={styles.Createdby}>
            <Text style={styles.TitleItem}>Created by :</Text>
            
            <View style={styles.CreatedbyItem}>
              <Icon style={styles.ItemCreatedby} name='gitlab' size= {25} color='rgba(0, 0, 0, 0.4)'/>
              <Text style={styles.TitleItem}>niken.pratiwi218</Text>
            </View>
            <View style={styles.CreatedbyItem}>
              <Icon style={styles.ItemCreatedby} name='instagram' size= {25} color='rgba(0, 0, 0, 0.4)'/>
              <Text style={styles.TitleItem}>nikenpcdp</Text>
            </View>
            <View style={styles.CreatedbyItem}>
              <Icon style={styles.ItemCreatedby} name='twitter' size= {25} color='rgba(0, 0, 0, 0.4)'/>
              <Text style={styles.TitleItem}>nikenpecedepe</Text>
            </View>
          </View>

          <TouchableOpacity style={styles.buttonClick} >
            <Button title='LOG OUT'
            color= '#F178CF'
            onPress={() => this.logoutHandler()} />
          </TouchableOpacity>

      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 60
  },
  logo: {
    marginVertical: 30
  },
  versionText: {
    fontSize: 12,
    textAlign: 'center'
  },
  TitleItem: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.4)',
    textAlign: 'center',
    marginVertical: 10
  },
  Createdby:{
    width: 180,
    height: 150,
    marginTop:50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  CreatedbyItem:{
    width: 130,
    height: 25,
    alignItems: 'center',
    marginVertical: 8,
    flexDirection: 'row'
  },
  ItemCreatedby:{
    marginRight: 15
  },
  buttonClick: {
    marginTop: 100,
    height: 36,
    width: 95,
    alignItems: 'center',
  }
});