import React from "react";
import {StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LoginPage from './LoginPage';
import Signup from './Signup';
import AboutUs from './AboutUs';
import HomeScreen from './HomeScreen';
import DetailPage from './DetailPage'


const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();


const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode='none'>
    <HomeStack.Screen name='HomeScreen' component={HomeScreen} />
    <HomeStack.Screen name='DetailPage' component={DetailPage}/>
  </HomeStack.Navigator>

)
const TabsScreen = () => (
  <Tabs.Navigator headerMode='none' 
    initialRouteName='Home'
    tabBarOptions={{
      activeTintColor: '#e91e63',
      activeBackgroundColor: '#E5D8F7',
      inactiveBackgroundColor: '#B2A7C0',
      labelStyle: {
        fontSize: 12,
        textAlign: "center",
        paddingBottom: 15,
        justifyContent: 'center',
        alignItems: 'center'
      }
    }}>
    <Tabs.Screen name='HOME' component={HomeStackScreen}/>
    <Tabs.Screen name='ABOUT US' component={AboutUs} />
  </Tabs.Navigator>
)

export default class Navigate extends React.Component{
  render(){
    return(
      <NavigationContainer>
        <RootStack.Navigator initialRouteName='Signup' headerMode='none'>
          <RootStack.Screen
            name='LoginPage'
            component={LoginPage}
          />
          <RootStack.Screen
            name='Signup'
            component={Signup}
            />
          <RootStack.Screen
            name= 'TabsScreen'
            component= {TabsScreen}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    )
  }
};