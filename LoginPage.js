import React from 'react';
import { View, Text, TextInput, StyleSheet, Image, Button, TouchableOpacity } from 'react-native';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  
  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
    if (this.state.password.length > 0 && this.state.userName.length > 0) {

        this.props.navigation.navigate('TabsScreen', {
        userName : this.state.userName
      })
    } else {
      
      this.setState({ isError: true });
    }

  }

  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.logo}>
          <Image source={require('./image/Logo.png')} style={{width: 240, height:65}} />
          </View> 
          <Text style={styles.subText}>Wellcome Back!</Text>
          <Text style={styles.quoteText}>Make your day with formulicious!</Text>
        

        <View style={styles.formContainer}>
          <View style={styles.tabPage}>
          <TouchableOpacity style={styles.buttonpage} onPress={() => {this.props.navigation.navigate('Signup')}}>
            <Text style={{fontSize: 20, color: 'rgba(0, 0, 0, 0.3)'}}>Signup</Text>
            <View style={{
              borderBottomColor: 'rgba(241, 120, 207, 0.3)',
              borderBottomWidth: 3,
              width: 73
            }}>
            </View>
          </TouchableOpacity>
          <View style={styles.buttonpage2}>
            <Text style={{color: 'black', fontSize: 20}}>Login</Text>
            <View style={{
              borderBottomColor: '#F178CF',
              borderBottomWidth: 3,
              width: 73
            }}>
            </View>
          </View>
          </View>

          <View style={styles.subContainer}>
            <View style={styles.inputContainer}>
              <View>
                <TextInput
                  style={styles.textInput}
                  placeholder='Username'
                  underlineColorAndroid='#B0B0B0'
                  onChangeText={userName => this.setState({ userName })}
                />
              </View>
            </View>
            <View style={styles.inputContainer}>
              <View>
                <TextInput
                  style={styles.textInput}
                  placeholder='Password'
                  underlineColorAndroid='#B0B0B0'
                  onChangeText={password => this.setState({ password })}
                  secureTextEntry={true}
                />
              </View>
            </View>
            <TouchableOpacity >
              <Text style={styles.ForgotText}>Forgot Password?</Text>
            </TouchableOpacity>
          </View>
          
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Enter a Random Password </Text>
          <TouchableOpacity style={styles.buttonClick} >
            <Button title='Login'
            color= '#F178CF'
            onPress={() => this.loginHandler()} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E6D2FF'
  },
  logo: {
    marginVertical: 30
  },
  subText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  quoteText: {
    fontSize: 10,
    textAlign: 'center'
  },
  formContainer: {
    marginTop: 30,
    height: 320,
    width: 280,
    borderRadius: 30,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 10,
    justifyContent: 'flex-end'
  },
  tabPage: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    
  },
  buttonpage: {
    width: 73,
    height:33,
    alignItems: 'center'
  },
  buttonpage2: {
    width: 73,
    height:33,
    alignItems: 'center'
  },
  subContainer: {
    marginVertical: 20
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    width: 242,
    height: 60
  },
  textInput: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.5)',
    width: 220
  },
  ForgotText: {
    fontSize: 12,
    color: '#F178CF',
    alignSelf: 'flex-end'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  },
  buttonClick: {
    height: 36,
    width: 95,
    alignItems: 'center',
    alignSelf: 'center'
  }
})