
Petunjuk Aplikasi:
Formulicious merupakan sebuah aplikasi mobile development dengan react-native yang berisikan tentang berbagai macam jenis resep makanan

API yang digunakan:
https://www.themealdb.com/api/json/v1/1/filter.php?c=${categories}
https://www.themealdb.com/api/json/v1/1/lookup.php?i=${idMeal}

Mockup https://www.figma.com/file/20KpcImHMwywaaozHy5wMR/Formulicious?node-id=0%3A1
Preview App Formulicious dan Apk https://drive.google.com/drive/folders/1SCkh3yeVdjx7AAKsqniIrurTC2y4KGqn?usp=sharing
Repo in Gitlab https://gitlab.com/niken.pratiwi218/formulicious-project

Terdiri dari 5 halaman utama:
1. Sign Up (Signup.js)
    Untuk user membuat akun, dengan syarat harus mengisi password
    Dalam aplikasi ini username dan password tidak disimpan ke dalam database, sehingga user dapat memasukkan data secara acak
2. Log In (LoginPage.js)
    Untuk user yang telah memiliki akun, dengan syarat harus mengisi password
    Dalam aplikasi ini username dan password tidak disimpan ke dalam database, sehingga user dapat memasukkan data secara acak
3. Home (HomeScreen.js)
    Merupakan halaman pertama setelah user melakukan sign up atau login
    Menampilkan berbagai macam menu makanan berdasarkan 14 kategori yang tersedia (contoh: beef, chicken, dessert)
4. Detail Page (DetailPage.js)
    Merupakan resep terperinci dari menu makanan yang dipilih di Home
    Terdapat judul makanan, asal daerah makanan, bahan-bahan, dan cara pembuatan
5. About Us (AboutUs.js)
    Halaman tentang formulicous app.