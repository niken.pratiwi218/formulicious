import React from 'react';
import {View, Text, StyleSheet, FlatList,Image,  TouchableOpacity} from 'react-native';
import {Picker} from '@react-native-community/picker';
import Axios from 'axios';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: 'beef',
      meals: []
    }
  }
  
  componentDidMount(){
    this.getMealsList()
  }

  getMealsList =async () => {
    const categories = this.state.categories
    try{
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${categories}`)
      this.setState({meals : response.data.meals})
    }catch (error) {
      console.log(error)
    }
  }

  handlePickerChange = async (itemValue) => {
    await this.setState({ categories : itemValue }) 
    this.getMealsList()
  }

  navigateToDetailPage (idMeal) {
    this.props.navigation.navigate
        ('DetailPage', {idMeal})
  }

  render(){
    console.log(this.props)
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
        <View style={styles.leftItemBar}>
            <Text>Hai,{'\n'}
             <Text style={styles.headerText}>Welcome back!</Text>
            </Text>
          <View style={styles.picker}>
          <Picker
                selectedValue={this.state.categories}
                style={{height:30, 
                  width: 160,
                }}
                itemStyle={{
                  justifyContent:'flex-start',
                  fontSize: 12
                }}
                mode='dropdown'
                onValueChange={
                  (itemValue) => this.handlePickerChange(itemValue)  
                }
                >
                  <Picker.Item label= 'Beef' value= 'beef'/>
                  <Picker.Item label= 'Chicken' value= 'chicken' />
                  <Picker.Item label= 'Dessert' value='dessert'/>
                  <Picker.Item label= 'Lamb' value= 'lamb'/>
                  <Picker.Item label= 'Miscellaneous' value= 'miscellaneous'/>
                  <Picker.Item label= 'Pasta' value= 'pasta'/>
                  <Picker.Item label= 'Pork' value= 'pork'/>
                  <Picker.Item label= 'Seafood' value= 'seafood'/>
                  <Picker.Item label= 'Side' value='side'/>
                  <Picker.Item label= 'Starter' value= 'starter'/>
                  <Picker.Item label= "Vegan" value= 'vegan'/>
                  <Picker.Item label= 'Vegetarian' value= 'vegetarian'/>
                  <Picker.Item label= 'Breakfast' value= 'breakfast'/>
                  <Picker.Item label= 'Goat' value= 'goat'/>
                </Picker>
          </View> 
        </View>

          <View style={styles.rightNav}>
            <View style={styles.logo}>
              <Image source={require('./image/Logo.png')} style={{width:140, height: 45}}/>
            </View>
            <Text style={styles.quoteText}>Make your day with formulicious!</Text>
          </View>
        </View>
          <View style={styles.body}>
            <FlatList
              data = {this.state.meals}
              renderItem={({item}) => <ListMeals navigateToDetailPage={this.navigateToDetailPage.bind(this)} data={item}/> }
              keyExtractor={item => item.idMeal}
              numColumns={2}
            />
          </View>
      </View>
    )
  }
};

class ListMeals extends React.Component {
  
  render(){
    const data = this.props.data
    return(
      <TouchableOpacity style= {styles.itemContainer} 
        onPress={()=> this.props.navigateToDetailPage(data.idMeal)}>

        <Image source={{uri:data.strMealThumb}} style={{width:180, height:100,borderRadius:20, opacity: .5}}/>
        
        <Text style={styles.mealsTitle}>{data.strMeal}</Text>
      </TouchableOpacity>
    )
  }
}

const styles= StyleSheet.create({
  container: {
    flex:1,
  },
  navBar: {
    height: 100,
    width: '100%',
    backgroundColor: '#E5D8F7',
    elevation: 5,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  rightNav: {
    alignContent: 'flex-end',
    alignItems: 'center',
  },
  logo: {
    alignSelf: 'flex-end',
    marginBottom: 5
  },
  quoteText: {
    fontSize: 8,
    color:"black"
  },
  picker: {
    height:30, 
    width: 160,
    backgroundColor:'white',
    borderRadius: 20,
    marginTop: 5
  },
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemContainer:{
    width: 180,
    height:100,
    borderRadius: 20,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    margin: 5
  },
  mealsTitle: {
    position: 'absolute',
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center'
  }
});