import React from 'react';
import {View, Text, Image, ScrollView, StyleSheet } from 'react-native';
import Axios from 'axios';


export default class DetailPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mealDetails: {}
    }
  }

  componentDidMount(){
    this.getMealDetails()
  }
  
  getMealDetails = async () => {
    const {idMeal} = this.props.route.params
    try{
      const response = await Axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${idMeal}`)
      this.setState({mealDetails : response.data.meals[0]})
      console.log(this.state.mealDetails)
    }catch (error) {
      console.log(error)
    }
  }


  render(){
    let mealDetails = this.state.mealDetails
    let arrIngredient = []
    let arrMeasure = []
    for(let i = 1 ; i<= 20 ; i ++){
      let itemIngredient = `strIngredient${i}`
      let itemMeasure = `strMeasure${i}`
      if(mealDetails[itemIngredient]){
        arrIngredient.push(mealDetails[itemIngredient])
        arrMeasure.push(mealDetails[itemMeasure])
      }
    }

    return (
      <ScrollView style = {styles.container}>
        <Image source={{uri:mealDetails.strMealThumb}} style={{width:'100%', height: 200}} />
        <View style ={styles.body}>
          <Text style={styles.categories}>{mealDetails.strCategory}</Text>
          <Text style={styles.TitleMeal}>{mealDetails.strMeal}</Text>
          <Text style={styles.AreaMeal}>{mealDetails.strArea}</Text>
          <View style={{
            borderBottomColor: "#F178CF",
            borderBottomWidth: 1,
            marginVertical: 15,
            width: '100%'
          }} />
          <Text style={styles.ingridients}>Ingredients</Text>
          <View style={styles.ingridientsContainer}>
            <View style={styles.ingridientsMeasure}>
              {arrMeasure.map((item,index) => {
                return(
                  <Text style = {styles.ingridientsMeasureTitle} key={index}> {item} </Text>
                )
              })}
            </View>
            <View style={styles.ingridientsDetail}>
              {arrIngredient.map((item,index) => {
                return(
                  <Text style={styles.ingridientsDetailTitle} key={index}> {item} </Text>
                )
              })}
            </View>
          </View>
          <View style={{
            borderBottomColor: "#F178CF",
            borderBottomWidth: 1,
            marginVertical: 15,
            width: '100%'
          }} />
          <Text style={styles.method}>Method</Text>
            
          <Text style={styles.methodDetail}>{mealDetails.strInstructions}</Text>
        </View>
      </ScrollView>
    )
  };
};

const styles =StyleSheet.create ({
  container: {
    flex: 1,
  },
  body: {
    alignItems: 'flex-start',
    margin: 10,
    justifyContent: 'space-between'
  },
  categories: {
    fontSize: 16,
    color: 'rgba(0, 0, 0, 0.5)'
  },
  TitleMeal: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 5
  },
  AreaMeal: {
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.7)'
  },
  ingridients:{
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black'
  },
  ingridientsContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 10
  },
  ingridientsMeasureTitle: {
    fontSize: 14,
    fontWeight: '700',
    margin: 3
  },
  ingridientsDetail:{
    width:200,
  },
  ingridientsDetailTitle: {
    fontSize: 14,
    margin: 3
  },
  method: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 10
  },
  methodClick: {
    fontSize: 16,
    fontWeight: '300',
    marginBottom: 10
  },
  methodDetail: {
    fontSize: 14,
    textAlign: 'justify',
    margin: 5
  }
});